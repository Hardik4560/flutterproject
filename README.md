## About Chope Merchant Notification
Chope Merchant Notification is an app to notify restaurant owners/managers about updates related to restaurant reservations via firebase push notifications and in-app history of notifications.

## Resources & links for learning flutter and setting up the development environment

Official Dart Programming Language Docs: https://dart.dev/guides 

Official Flutter Docs: https://flutter.io/docs/

macOS Setup Guide: https://flutter.io/setup-macos

Windows Setup Guide: https://flutter.io/setup-windows

Linux Setup Guide: https://flutter.io/setup-linux

Visual Studio Code: https://code.visualstudio.com/

Visual Studio Code Flutter Extension: https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter

Android Studio: https://developer.android.com/studio/

Firebase: https://firebase.google.com/docs/guides

Firebase Console: https://console.firebase.google.com/u/0/project/chope-notification/

Amplitude API: https://www.docs.developers.amplitude.com/getting-started/


## Steps to run the app locally

**Step 1:**

Download or clone this repo by using the link below:

```
https://gitlab.chope.it/frontend/mobile/merchant-notification-app.git
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies: 

```
flutter pub get 
```

**Step 3:**

Execute the following command to see if there are any issues with the flutter setup:

```
flutter doctor
```

**Step 3:**

Execute the following command to run the project:

```
flutter run
```


## Folder Structure

```
root/
|- android
|- build
|- ios
|- lib
```

Folder structure inside lib

```
lib/
|- constants/
|- widgets/
|- firebase_options.dart
|- main.dart
|- palette.dart
```

"android/" folder consists of configurations specific to Android platform.
"build/" folder consists of the build outputs for all the platforms.
"ios/" folder consists of configurations specific to iOS platform.
"lib/" folder consists of all the source code written using Dart and Flutter.

## UI designs reference resources for the app
**Resource 1:**
https://www.figma.com/file/9JeRAy8FYwhWeIfd4uIwNX/Notification-flow-%2F-Flutter-%2F-Notification-app?node-id=1%3A3

**Resource 2:**
https://www.figma.com/file/9JeRAy8FYwhWeIfd4uIwNX/Notification-flow-%2F-Flutter-%2F-Notification-app?node-id=1%3A25


## Build the app locally
**Step 1:**
Toggle the boolean value of flag 'isProductionBuild' from /lib/constants/constants.dart, based on whether you want to trigger a production build or a UAT build.

**Step 2:**
For Android, run the following command from the project's root folder to create a build:
```
'flutter build apk'
```

For iOS, run the following command from the project's root folder to create a build:
```
'flutter build ios'
```

**Step 3:**
You can fetch the build file outputs from the following path:
For Android: 'build/app/outputs/flutter-apk/app-release.apk'
For iOS: '/build/ios/iphoneos/Runner.app'


## Build and release the Android app
**Step 1:**
Run the following command from the project's root folder to create a build that can be uploaded to Google Play Store:
```
'flutter build appbundle'
```

**Step 2:**
Fetch the build from here: '/build/app/outputs/bundle/release/app-release.aab'

**Step 3:**
Follow the steps here: https://support.google.com/googleplay/android-developer/answer/9859348?hl=en

## Build and release the iOS app
**Step 1:**
Open your project using Xcode.

**Step 2:**
Select 'Product' dropdown from the top menu and select 'Archive' from the dropdown.

**Step 3:**
If the archive is successful, the generated build will be shown in the archives pop-up. Select the build and click on 'Validate App' to check for any potential validation errors.

**Step 4:**
Once the app passes the validation, click on the "Distribute App" and select the following:-
Method of distribution: "App Store Connect"
Destination: Upload
Then click on "Next" to proceed to distribution options and keep the defaults.
Then click on "Next" to proceed to Sign/Re-sign the Runner and select "Automatically manage signing".
Then click on "Next" and wait for the distribution ready archive to be generated. Once the process is finished, click on "Upload". This will upload an ipa file to Apple appstoreconnect.

**Step 5**
Submit the app for review using App Store tab or release the app for internal testing using TestFlight tab on appstoreconnect dashboard here: https://appstoreconnect.apple.com/

**Step 6**
Once the app is approved by Apple team, it can be released to the app store. To learn more about App Store Connect, please visit: https://developer.apple.com/support/app-store-connect/

Thank you for reading!