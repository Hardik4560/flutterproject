// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables

//package import
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:url_launcher/url_launcher.dart';

//file import
import '../constants/constants.dart';

class Notifications extends StatefulWidget {
  var notifications;
  var fetchNotifications;
  var hasMore;
  var forceRefresh;

  Notifications(
      List this.notifications,
      Future<void> Function(dynamic isTriggeredByPagination)
          this.fetchNotifications,
      bool this.hasMore,
      Future<void> Function() this.forceRefresh,
      {Key? key})
      : super(key: key);

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  var items = [];

  final controller = ScrollController();

  void _buildNotificationItems() {
    items = List.generate(widget.notifications.length, (i) {
      return NotificationListHeaderItem('${widget.notifications[i]['date']}',
          widget.notifications[i]["notifications"]);
    });
  }

  @override
  void initState() {
    controller.addListener(() {
      if (controller.position.pixels >= controller.position.maxScrollExtent) {
        widget.fetchNotifications(true);
      }
    });
    _buildNotificationItems();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(Notifications oldWidget) {
    _buildNotificationItems();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(notificationsLabel),
            centerTitle: false,
            automaticallyImplyLeading: false,
            actions: <Widget>[
              IconButton(
                icon: Image.asset('assets/images/chope_c_logo.png'),
                onPressed: () {
                  // do something
                },
              ),
            ]),
        body: items.isNotEmpty
            ? Container(
                color: const Color(0xFFE5E5E5),
                child: RefreshIndicator(
                    onRefresh: () async {
                      return await widget.forceRefresh();
                    },
                    child: ListView.builder(
                        controller: controller,
                        // Let the ListView know how many items it needs to build.
                        itemCount: items.length + 1,
                        itemBuilder: (context, index) {
                          if (index < items.length) {
                            final item = items[index];

                            return StickyHeader(
                              header: Container(
                                padding:
                                    const EdgeInsets.only(top: 16, bottom: 8.0),
                                color: const Color(0xFFE5E5E5),
                                child: item.buildHeader(context),
                              ),
                              content: Column(
                                  children: List.generate(
                                      item.notifications.length, (i) {
                                return Padding(
                                  padding: EdgeInsets.only(
                                      top: i == 0 ? 0.0 : 8.0,
                                      left: 16.0,
                                      right: 16.0,
                                      bottom: 8.0),
                                  child: Card(
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                      ),
                                      child: ListTile(
                                        title: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 16,
                                                left: 16.0,
                                                right: 16.0),
                                            child: Text(
                                              item.notifications[i]['date'],
                                              style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Color(0xff333333)),
                                            )),
                                        subtitle: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 16,
                                              bottom: 16.0,
                                              left: 16.0,
                                              right: 16.0),
                                          child: SelectableLinkify(
                                            onOpen: (link) async {
                                              if (await canLaunch(link.url)) {
                                                await launch(link.url);
                                              } else {
                                                throw 'Could not launch $link';
                                              }
                                            },
                                            text: item.notifications[i]
                                                ['content'],
                                            style: const TextStyle(
                                              fontSize: 14,
                                              color: Color(0xff333333),
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),
                                      )),
                                );
                              })),
                            );
                          } else {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Center(
                                child: widget.hasMore
                                    ? const CircularProgressIndicator()
                                    : const Text(noMoreNotificationsLabel),
                              ),
                            );
                          }
                        })))
            : Center(
                child: Image.asset(
                  'assets/images/no_reservations.png',
                  fit: BoxFit.fitWidth,
                ),
              ));
  }
}

/// A ListItem that contains data to display a message.
class MessageItem {
  final String date;
  final String message;

  MessageItem(this.date, this.message);

  Widget buildTitle(BuildContext context) => Text(
        date,
        style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
            color: Color(0xff333333)),
      );

  Widget buildSubtitle(BuildContext context) => Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Text(
          message,
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xff333333),
            fontWeight: FontWeight.normal,
          ),
        ),
      );
}

class NotificationListHeaderItem {
  final String date;
  final List notifications;

  NotificationListHeaderItem(this.date, this.notifications);

  Widget buildHeader(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Text(
        date,
        style: const TextStyle(
            fontSize: 18,
            color: Color(0xff404040),
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
