// ignore_for_file: deprecated_member_use, constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, avoid_print, use_build_context_synchronously
import 'dart:convert';
import 'package:amplitude_flutter/amplitude.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

import './permissions.dart';
import '../constants/constants.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen(String? this.fcmToken, {Key? key}) : super(key: key);
  // ignore: prefer_typing_uninitialized_variables
  final fcmToken;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg_m.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  flex: deviceSize.width > 600 ? 2 : 1,
                  child: AuthCard(fcmToken),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard(String? this.fcmToken, {Key? key}) : super(key: key);
  // ignore: prefer_typing_uninitialized_variables
  final fcmToken;

  @override
  AuthCardState createState() => AuthCardState();
}

class AuthCardState extends State<AuthCard> {
  //Check if user is logged in already. This is helpful when the app is killed.
  Future<void> _checkIfLoggedIn() async {
    final preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey('userDetails')) {
      final userDetails = json.decode(preferences.getString('userDetails')!)
          as Map<String, dynamic>;
      userId = userDetails['userId'];
      _authData['accessToken'] = userDetails['accessToken'];
      _authData['refreshToken'] = userDetails['refreshToken'];
      receiverId = userDetails['receiverId'];
      _authData['receiverId'] = receiverId;
      _authData['fcmToken'] = userDetails['fcmToken'];
      Navigator.pop(context);
      Navigator.of(context).push(MaterialPageRoute(builder: (_) {
        return PermissionsScreen(userId, _authData['accessToken'],
            _authData['fcmToken'], receiverId);
      }));
    } else {
      // Report to analytics
      Amplitude.getInstance().logEvent('view log in page',
          eventProperties: {"chope platform": "notification app"});
    }
  }

  @override
  void initState() {
    _checkIfLoggedIn();
    super.initState();
  }

  final GlobalKey<FormState> _formKey = GlobalKey();
  final Map<String, String> _authData = {
    'username': '',
    'password': '',
    'companyId': '',
    'fcmToken': '',
    'accessToken': '',
    'refreshToken': '',
    'receiverId': '',
  };
  final _passwordController = TextEditingController();
  var ruids = [];
  var receiverId = '';
  var userId = 0;
  var _errorMessage = '';
  var isLoading = false;
  var _passwordVisible = false;

  Future<void> _submit() async {
    if (isLoading) {
      //Avoid multiple log in requests for multiple log in button clicks
      return;
    }
    isLoading = true;
    setState(() {
      _errorMessage = '';
    });
    if (!_formKey.currentState!.validate()) {
      // Invalid!
      isLoading = false;
      return;
    } else {
      _formKey.currentState!.save();
    }

    //Login endpoint
    var url = Uri.parse(loginEndpoint);
    var response = await http.post(url, body: {
      'username': _authData['username'],
      'password': _authData['password'],
      'group_uid': _authData['companyId'],
      'platform': 'cloud_mo',
      'build': '7.0.0'
    });

    var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    var statusCode = decodedResponse['status']['code'];
    String? statusMsg = decodedResponse['status']['msg'];

    //If login successful
    if (statusCode == "0") {
      var restaurants = decodedResponse['result']['restaurants'];
      for (String key in restaurants.keys) {
        ruids.add(key);
      }
      _authData['accessToken'] =
          decodedResponse['result']['token_infos']['access_token'];
      _authData['refreshToken'] =
          decodedResponse['result']['token_infos']['refresh_token'];

      //Access token validation and user details endpoint
      var path =
          Uri.parse('$tokenVerificationEndpoint${_authData['accessToken']}');
      var result = await http.post(path);
      var decodedResult = jsonDecode(utf8.decode(result.bodyBytes)) as Map;

      //If token validation successful
      if (decodedResult['status']['code'] == "0") {
        setState(() {
          userId = decodedResult['result']['user_id'];
        });
      }

      var queryParameters = {
        'user-id-type': 'mr',
      };
      //Register receiver (for push notifications) endpoint
      var endpoint = Uri.https(
          receiverEndpoint, '$receiverEndpointPath$userId', queryParameters);
      var receiverRes = await http.put(endpoint,
          headers: {
            'Authorization': 'Bearer ${_authData['accessToken']}',
          },
          body: jsonEncode({
            'token_id': widget.fcmToken,
            'token_type': 'push',
            'ruids': ruids
          }));
      var decodedReceiverRes =
          jsonDecode(utf8.decode(receiverRes.bodyBytes)) as Map;
      receiverId = decodedReceiverRes['data']['receiver_id'];
      final preferences = await SharedPreferences.getInstance();
      final userDetails = json.encode({
        'userId': userId,
        'receiverId': receiverId,
        'accessToken': _authData['accessToken'],
        'refreshToken': _authData['refreshToken'],
        'username': _authData['username'],
        'password': _authData['password'],
        'companyId': _authData['companyId'],
        'fcmToken': widget.fcmToken,
      });
      preferences.setString('userDetails', userDetails);

      // Report to analytics
      Amplitude.getInstance().logEvent('click log in button', eventProperties: {
        "pre-login company id": _authData['companyId'],
        "pre-login user id": _authData['username'],
        "log in result string": statusMsg,
        "log in result code": statusCode,
        "chope platform": "notification app",
        "company id": _authData['companyId'],
        "user id": userId
      });

      Navigator.pop(context);
      Navigator.of(context).push(MaterialPageRoute(builder: (_) {
        return PermissionsScreen(
            userId, _authData['accessToken'], widget.fcmToken, receiverId);
      }));
    } else if (statusMsg!.isNotEmpty) {
      setState(() {
        //Validation error message coming from the server
        _errorMessage = statusMsg;
        _formKey.currentState!.validate();
        _passwordVisible = false;
      });
    }
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        height: deviceSize.height * 0.82,
        constraints: const BoxConstraints(minHeight: 260),
        width: deviceSize.width * 0.85,
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Container(
            child: Stack(children: [
              Column(
                children: <Widget>[
                  const SizedBox(height: 35),
                  Image.asset(
                    'assets/images/chope_combined_logo.png',
                  ),
                  const SizedBox(height: 25),
                  const Text(
                    notificationsLabel,
                    style: TextStyle(
                      color: Color.fromARGB(255, 16, 2, 74),
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    decoration: const InputDecoration(
                        isDense: true,
                        helperText: companyIdHelperText,
                        helperMaxLines: 3,
                        border: OutlineInputBorder(),
                        hintText: companyIdHintText),
                    keyboardType: TextInputType.text,
                    onSaved: (value) {
                      _authData['companyId'] = value!;
                    },
                  ),
                  const SizedBox(height: 12),
                  TextFormField(
                    decoration: const InputDecoration(
                        isDense: true,
                        border: OutlineInputBorder(),
                        hintText: userIdHintText),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return userIdEmptyMsg;
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _authData['username'] = value!;
                    },
                  ),
                  const SizedBox(height: 14),
                  TextFormField(
                    decoration: InputDecoration(
                      isDense: true,
                      border: const OutlineInputBorder(),
                      hintText: passwordHintText,
                      // Here is key idea
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      ),
                    ),
                    obscureText: !_passwordVisible,
                    controller: _passwordController,
                    validator: (value) {
                      if (value!.isEmpty || _errorMessage.isNotEmpty) {
                        return _errorMessage.isNotEmpty
                            ? '\u26A0 $_errorMessage'
                            : passwordEmptyMsg;
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _authData['password'] = value!;
                    },
                  ),
                  Link(
                      uri: Uri.parse(forgotPasswordLink),
                      builder: (_, followLink) {
                        return Align(
                          alignment: Alignment.topRight,
                          child: FlatButton(
                            onPressed: followLink,
                            textColor: const Color.fromARGB(255, 77, 77, 208),
                            child: const Text(forgotPasswordLabel),
                          ),
                        );
                      }),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.all(5),
                  width: double.infinity,
                  color: const Color.fromARGB(255, 16, 2, 74),
                  child: ButtonTheme(
                    minWidth: deviceSize.width * 0.85,
                    child: RaisedButton(
                      onPressed: _submit,
                      color: const Color.fromARGB(255, 16, 2, 74),
                      textColor:
                          Theme.of(context).primaryTextTheme.button!.color,
                      child: const Text(loginButtonLabel),
                    ),
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
