// ignore_for_file: prefer_typing_uninitialized_variables, use_build_context_synchronously, deprecated_member_use

//package imports
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/link.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:amplitude_flutter/amplitude.dart';
import 'package:shared_preferences/shared_preferences.dart';

//file import
import './auth_screen.dart';
import '../constants/constants.dart';

class Settings extends StatefulWidget {
  final userId;
  final accessToken;
  final fcmToken;
  final receiverId;
  const Settings(this.userId, this.accessToken, this.fcmToken, this.receiverId,
      {Key? key})
      : super(key: key);
  @override
  State<Settings> createState() => SettingsState();
}

class SettingsState extends State<Settings> {
  @override
  void initState() {
    super.initState();
  }

  void _showLogoutConfirmationDialog() {
    showCupertinoDialog(
        context: context,
        builder: (BuildContext ctx) {
          return CupertinoAlertDialog(
            title: const Text(logoutConfirmMsg),
            actions: [
              CupertinoDialogAction(
                onPressed: _logout,
                isDefaultAction: true,
                isDestructiveAction: true,
                child: const Text(yesButtonLabel),
              ),
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                isDefaultAction: false,
                isDestructiveAction: false,
                child: const Text(noButtonLabel),
              )
            ],
          );
        });
  }

  Future<void> _logout() async {
    final preferences = await SharedPreferences.getInstance();

    //Get the user details.
    var userDetails = json.decode(preferences.getString('userDetails')!)
        as Map<String, dynamic>;

    var queryParameters = {
      'receiver-id': widget.receiverId,
      'user-id-type': 'mr',
    };
    var endpoint = Uri.https(receiverEndpoint,
        '$receiverEndpointPath${userDetails['userId']}', queryParameters);

    //Try deleting the user.
    var response = await http.delete(endpoint, headers: {
      'Authorization': 'Bearer ${userDetails['accessToken']}}',
    });

    var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    if (decodedResponse['error'] != null &&
        decodedResponse['error']['status_code'] == 401) {
      //The token has expired, can't delete the user. Refresh the token but logging in the user and then logout.
      var userDetails = json.decode(preferences.getString('userDetails')!)
          as Map<String, dynamic>;
      var url = Uri.parse(loginEndpoint);
      var response = await http.post(url, body: {
        'username': userDetails['username'],
        'password': userDetails['password'],
        'group_uid': userDetails['companyId'],
        'platform': 'cloud_mo'
      });

      var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
      final newUserDetails = json.encode({
        'userId': userDetails['userId'],
        'receiverId': userDetails['receiverId'],
        'accessToken': decodedResponse['result']['token_infos']['access_token'],
        'refreshToken': userDetails['refreshToken'],
        'username': userDetails['username'],
        'password': userDetails['password'],
        'companyId': userDetails['companyId'],
        'fcmToken': widget.fcmToken,
      });
      preferences.setString('userDetails', newUserDetails);
      _logout();
      return;
    }

    //Delete user was successful. Clear the prefs and take the user to auth screen.
    if (decodedResponse['success'] == true) {
      // Report to analytics
      Amplitude.getInstance()
          .logEvent('notification app - logout', eventProperties: {
        "company id": userDetails['companyId'],
        "user id": userDetails['userId'],
        "logout_success": true,
      });

      preferences.clear();
      Navigator.pop(context);
      Navigator.of(context).push(MaterialPageRoute(builder: (_) {
        return AuthScreen(widget.fcmToken);
      }));
    } else {
      //Delete user failed, log it and inform the user.
      // Report to analytics
      Amplitude.getInstance()
          .logEvent('notification app - logout', eventProperties: {
        "company id": userDetails['companyId'],
        "user id": userDetails['userId'],
        "logout_success": false,
        "server_response": decodedResponse
      });

      Fluttertoast.showToast(
        msg: logoutErrorMsg,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(settingsLabel, style: TextStyle(color: Colors.black)),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: [
          //This code can be uncommented while implementing SGTA-359
          // ListTile(
          //   title: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       Padding(
          //         padding: const EdgeInsets.only(left: 15),
          //         child: const Text(enablePushNotificationsLabel,
          //             style: TextStyle(
          //               color: Color.fromARGB(255, 37, 20, 108),
          //             )),
          //       ),
          //       CupertinoSwitch(
          //         value: true,
          //         onChanged: (bool value) {},
          //       ),
          //     ],
          //   ),
          // ),
          if (Theme.of(context).platform == TargetPlatform.android)
            ListTile(
              title: Link(
                  uri: Uri.parse(dontKillMyAppLink),
                  builder: (_, followLink) {
                    return Align(
                      alignment: Alignment.centerLeft,
                      child: FlatButton(
                        onPressed: followLink,
                        textColor: const Color.fromARGB(255, 37, 20, 108),
                        child: const Text(dontKillMyAppLabel,
                            style: TextStyle(fontSize: 16)),
                      ),
                    );
                  }),
            ),
          ListTile(
            onTap: _showLogoutConfirmationDialog,
            title: Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(
                children: const [
                  Icon(Icons.logout_outlined),
                  Text(
                    logoutButtonLabel,
                    style: TextStyle(
                      color: Color.fromARGB(255, 211, 47, 47),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
