// ignore_for_file: deprecated_member_use, constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, prefer_typing_uninitialized_variables, use_build_context_synchronously

//package imports
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:permission_handler/permission_handler.dart';

//file import
import './app.dart';
import '../constants/constants.dart';

class PermissionsScreen extends StatelessWidget {
  final userId;
  final accessToken;
  final fcmToken;
  final receiverId;
  const PermissionsScreen(
      this.userId, this.accessToken, this.fcmToken, this.receiverId,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'assets/images/bg_m.png',
            fit: BoxFit.cover,
            height: deviceSize.height,
            width: deviceSize.width,
            alignment: Alignment.center,
          ),
          SingleChildScrollView(
            child: Container(
              height: deviceSize.height,
              width: deviceSize.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: deviceSize.width > 600 ? 2 : 1,
                    child: PermissionsCard(
                        userId, accessToken, fcmToken, receiverId),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PermissionsCard extends StatefulWidget {
  final userId;
  final accessToken;
  final fcmToken;
  final receiverId;
  const PermissionsCard(
    this.userId,
    this.accessToken,
    this.fcmToken,
    this.receiverId, {
    Key? key,
  }) : super(key: key);

  @override
  PermissionsCardState createState() => PermissionsCardState();
}

class PermissionsCardState extends State<PermissionsCard> {
  @override
  void initState() {
    _requestPermissions();
    super.initState();
  }

  Future<void> _requestPermissions() async {
    Permission permission;
    permission = Permission.notification;
    var status = await permission.request().isGranted;
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    if (!status) {
      await messaging.requestPermission(
        alert: true,
        announcement: true,
        badge: true,
        sound: true,
      );
    } else {
      _redirectToApp();
    }
  }

  _redirectToApp() {
    Navigator.pop(context);
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return App(widget.userId, widget.accessToken, widget.fcmToken,
          widget.receiverId);
    }));
  }

  _okButtonHandler() {
    _requestPermissions();
    _redirectToApp();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        height: deviceSize.height * 0.82,
        constraints: const BoxConstraints(minHeight: 260),
        width: deviceSize.width * 0.85,
        padding: const EdgeInsets.all(16.0),
        child: Stack(
          children: [
            Column(
              children: [
                const SizedBox(height: 20),
                Image.asset(
                  'assets/images/chope_combined_logo.png',
                  width: deviceSize.width * 0.50,
                ),
                const SizedBox(height: 35),
                const Text(
                  enableNotificationsLabel,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 16, 2, 74),
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                const SizedBox(height: 20),
                const Text(
                  notificationsPermissionMsg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 51, 51, 51),
                    fontSize: 16,
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: const EdgeInsets.all(5),
                width: double.infinity,
                color: const Color.fromARGB(255, 16, 2, 74),
                child: ButtonTheme(
                  minWidth: deviceSize.width * 0.85,
                  child: RaisedButton(
                    onPressed: _okButtonHandler,
                    color: const Color.fromARGB(255, 16, 2, 74),
                    textColor: Theme.of(context).primaryTextTheme.button!.color,
                    child: const Text(okayButtonLabel),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
