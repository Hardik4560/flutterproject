// ignore_for_file: prefer_typing_uninitialized_variables, use_build_context_synchronously, deprecated_member_use

//package imports
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

//file imports
import './settings.dart';
import './notifications.dart';
import '../constants/constants.dart';

class App extends StatefulWidget {
  final userId;
  final accessToken;
  final fcmToken;
  final receiverId;
  const App(this.userId, this.accessToken, this.fcmToken, this.receiverId,
      {Key? key})
      : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  int _selectedIndex = 0;
  var notifications = [];
  var cursor;
  var hasMore = true;
  var _widgetOptions = <Widget>[const Text(''), const Text('')];

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    _fetchNotifications(false);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _fetchNotifications(false);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> _forceRefresh() {
    cursor = null;
    notifications.clear();

    return _fetchNotifications(false);
  }

  //_fetchNotifications method is called in the following scenarios:
  //1. User routed from the auth screen to notifications page (Fetch and show notifications)
  //2. App comes from the background to foreground (Fetch but update data only if there are new notifications)
  //3. Lazy load pagination (Fetch and append the the new chunk notifications to the existing list)
  Future<void> _fetchNotifications(isTriggeredByPagination) async {
    //Don't make http request to fetch notifications if the user has scrolled down until the last record
    if (!hasMore && isTriggeredByPagination) {
      return;
    }
    final preferences = await SharedPreferences.getInstance();
    var prefs = json.decode(preferences.getString('userDetails')!);
    var bearerToken = prefs['accessToken'];
    var notificationsQueryParams = {
      'msg-type': 'push',
      'limit': '20',
    };

    if (cursor != null && isTriggeredByPagination) {
      notificationsQueryParams['cursor'] = cursor;
    }

    //Fetch notifications endpoint
    var notificationsUrl = Uri.https(notificationsEndpoint,
        '$notificationsEndpointPath${widget.userId}', notificationsQueryParams);
    var notificationsResult = await http.get(
      notificationsUrl,
      headers: {
        'Authorization': 'Bearer $bearerToken',
      },
    );
    var decodedNotificationsResult =
        jsonDecode(utf8.decode(notificationsResult.bodyBytes)) as Map;

    //Code to relogin user when the token is expired.
    if (decodedNotificationsResult['error'] != null &&
        decodedNotificationsResult['error']['status_code'] == 401) {
      var userDetails = json.decode(preferences.getString('userDetails')!)
          as Map<String, dynamic>;
      var url = Uri.parse(loginEndpoint);
      var response = await http.post(url, body: {
        'username': userDetails['username'],
        'password': userDetails['password'],
        'group_uid': userDetails['companyId'],
        'platform': 'cloud_mo',
        'build': '7.0.0'
      });

      var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
      final newUserDetails = json.encode({
        'userId': userDetails['userId'],
        'receiverId': userDetails['receiverId'],
        'accessToken': decodedResponse['result']['token_infos']['access_token'],
        'refreshToken': userDetails['refreshToken'],
        'username': userDetails['username'],
        'password': userDetails['password'],
        'companyId': userDetails['companyId'],
        'fcmToken': widget.fcmToken,
      });
      preferences.setString('userDetails', newUserDetails);
      _fetchNotifications(false);
    }

    String parseDateTime(e) {
      var ddd = '${e['updated_at'].substring(0, 19) + '+00'}';
      var date = DateFormat('dd MMMM yyyy (EEE) h:mma')
          .format(DateTime.parse(ddd).toLocal());
      return date.replaceAll('AM', 'am').replaceAll('PM', 'pm');
    }

    setState(() {
      if (decodedNotificationsResult['success'] == true) {
        List<dynamic> newNotifications =
            (decodedNotificationsResult['data']['messages'])
                .map((e) => ({
                      'date': parseDateTime(e),
                      'content': e['content'],
                      'type': 'message',
                    }))
                .toList();
        //Add notification header for every new date
        var notificationsWithHeaders = [];
        var notificationMessageList = [];

        //get the unique dates
        var seen = <String>{};
        newNotifications.where((notication) {
          var date = notication['date'];
          //Get the last closing bracket.
          var indexOfClosingBracket = date.indexOf(')');
          var dateWithoutTime = date.substring(0, indexOfClosingBracket + 1);
          return seen.add(dateWithoutTime);
        }).toList();

        //TODO: BAD code, O(n)2, possible to reduce it to O(n) using calculation on server.
        //Parsing the list and creating a structure for header list view.
        for (var element in seen) {
          var date = element.toString().trim();

          for (var j = 0; j < newNotifications.length; j++) {
            var itemDate = newNotifications[j]['date'];
            var indexOfClosingBracket = itemDate.indexOf(')');

            if (date == itemDate.substring(0, indexOfClosingBracket + 1)) {
              notificationMessageList.add(newNotifications[j]);
            }
          }

          //Check if date is today.
          date = checkIfToday(date);

          //Create the header for the date.
          notificationsWithHeaders.add({
            'date': date,
            "notifications": notificationMessageList,
          });

          notificationMessageList = [];
        }

        //print(notificationsWithHeaders);

        //the latest fetched notifications from the server and the latest on the frontend are still the same.
        if (!isTriggeredByPagination &&
            notifications.isNotEmpty &&
            notificationsWithHeaders.isNotEmpty &&
            notifications[0]['date'] == newNotifications[0]['date'] &&
            notifications[0]['notifications']['content']
                    .compareTo(newNotifications[0]['content']) ==
                0) {
          return;
        }

        //Remove the duplicate notificartions. //TODO AVOID O(n)2
        for (var element in notifications) {
          for (var notification in element['notifications']) {
            if (notification['content']
                .contains(newNotifications[0]['content'])) {
              return;
            }
          }
        }

        if (isTriggeredByPagination) {
          var tempItemToRemove;
          //Check if the headers already has the date.
          for (var element in notifications) {
            var dateAlreadyPresent = element['date'];
            for (var newHeaderElements in notificationsWithHeaders) {
              if (dateAlreadyPresent == newHeaderElements['date']) {
                //Date is already present. Need to move the notification items to notifications list.
                //Add the new items to old list.
                element['notifications']
                    .addAll(newHeaderElements['notifications']);

                //Remove the header from the list.
                tempItemToRemove = newHeaderElements;
              }
            }
          }

          if (tempItemToRemove != null) {
            notificationsWithHeaders.remove(tempItemToRemove);
          }

          notifications.addAll(notificationsWithHeaders);
        } else {
          notifications = notificationsWithHeaders;
          hasMore = true;
        }

        if (decodedNotificationsResult['data']['exclusive_start_key'] == '') {
          hasMore = false;
        }
        cursor = decodedNotificationsResult['data']['exclusive_start_key'];
      }

      _widgetOptions = <Widget>[
        Notifications(
            notifications, _fetchNotifications, hasMore, _forceRefresh),
        Settings(widget.userId, widget.accessToken, widget.fcmToken,
            widget.receiverId)
      ];
    });
  }

  String checkIfToday(String date) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);

    final dateToCheck = DateFormat("dd MMMM yyyy (EEE)").parse(date);
    final aDate =
        DateTime(dateToCheck.year, dateToCheck.month, dateToCheck.day);
    if (aDate == today) {
      date = "Today, $date";
    }
    return date;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 10,
        unselectedFontSize: 10,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications_none),
            label: notificationsLabel,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings_outlined),
            label: settingsLabel,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromARGB(255, 77, 77, 208),
        onTap: _onItemTapped,
      ),
    );
  }
}
