//Change the below flag to true/false to toggle between the production and uat build
const isProductionBuild = false;

//Domains
const uat1 = 'zhuo88.com';
const uat2 = 'fandianwei.com';
const production = 'chope.co';

const domain1 = isProductionBuild ? production : uat1;
const domain2 = isProductionBuild ? production : uat2;

//Endpoints
const loginEndpoint = 'https://mrapi.$domain1/login/do_login';
const tokenVerificationEndpoint =
    'https://mrapi.$domain1/login/verify_token?token=';

const receiverEndpoint = 'ms-ordernotification.$domain2';
const receiverEndpointPath = '/v1/receivers/';

const notificationsEndpoint = 'ms-ordernotification.$domain2';
const notificationsEndpointPath = '/v1/notifications/mr/';

//Link
const forgotPasswordLink = 'https://mcloud.$domain1/#/signIn_password';
const dontKillMyAppLink = 'https://dontkillmyapp.com/';

//Labels, Helper Texts, Hint Texts & Messages
const appName = 'Chope Merchant Notifications';
const notificationsLabel = 'Notifications';
const settingsLabel = 'Settings';
const forgotPasswordLabel = 'Forgot Password';
const yesButtonLabel = 'Yes';
const noButtonLabel = 'No';
const loginButtonLabel = 'Log In';
const logoutButtonLabel = ' Logout';
const okayButtonLabel = 'Okay';
const enablePushNotificationsLabel = 'Enable push notifications ';
const companyIdHelperText =
    'If you have not received your assigned Company ID, please leave blank.';
const companyIdHintText = 'Company ID';
const userIdHintText = 'User ID';
const userIdEmptyMsg = 'User ID cannot be empty!';
const passwordHintText = 'Password';
const passwordEmptyMsg = 'Password cannot be empty!';
const enableNotificationsLabel = 'Enable notifications';
const logoutErrorMsg = 'Error logging out. Try again after some time.';
const dontKillMyAppLabel = 'Don\'t kill my app';
const logoutConfirmMsg = 'Are you sure you want to logout?';
const noMoreNotificationsLabel = '-- No more notifcations to show --';
const notificationsPermissionMsg =
    'In order to send you timely notifications on your new, edited and cancelled reservations, please enable notifications for this app.';
