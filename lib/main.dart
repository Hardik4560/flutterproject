// ignore_for_file: prefer_typing_uninitialized_variables

//package imports
import 'dart:developer';
import 'firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:amplitude_flutter/amplitude.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

//file imports
import './palette.dart';
import './widgets/auth_screen.dart';
import 'constants/constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  //Code for Crashanalytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  final fcmToken = await FirebaseMessaging.instance.getToken();
  log('fcm token 1: $fcmToken');
  FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) {
    log('fcm token: $fcmToken');
  }).onError((err) {
    log('fcm token err: $err');
  });

  // Initialize Amplitude Analytics
  Amplitude.getInstance().init('24e72816805846faf42e1cf873550afd');

  // Report to analytics if app opened directly
  Amplitude.getInstance()
      .logEvent('notification app - app open', eventProperties: {
    "source": 'direct open',
  });

  // Report to analytics if app opened by notifications
  FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    Amplitude.getInstance()
        .logEvent('notification app - app open', eventProperties: {
      "source": 'push notification',
    });
  });

  runApp(MyApp(fcmToken));
}

class MyApp extends StatelessWidget {
  const MyApp(String? this.fcmToken, {Key? key}) : super(key: key);

  final fcmToken;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Palette.primaryThemeColor,
        fontFamily: 'NotoSans',
      ),
      home: AuthScreen(fcmToken),
    );
  }
}
