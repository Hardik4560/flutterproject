//palette.dart

//package imports
import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor primaryThemeColor = MaterialColor(
    0xff10024A, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    <int, Color>{
      50: Color(0xff10024A), //10%
      100: Color(0xff10024A), //20%
      200: Color(0xff10024A), //30%
      300: Color(0xff10024A), //40%
      400: Color(0xff10024A), //50%
      500: Color(0xff10024A), //60%
      600: Color(0xff10024A), //70%
      700: Color(0xff10024A), //80%
      800: Color(0xff10024A), //90%
      900: Color(0xff10024A), //100%
    },
  );
}
